class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :dni
      t.string :nombre
      t.string :apellido
      t.string :address
      t.string :password_digest
      t.string :phone

      t.timestamps
    end
    add_index :users, :dni, unique: true
    add_index :users, :address, unique: true
  end
end
